flashCore.prototype.fadeOut = function(el, timing, callback) {
    var self = this;

    if(!timing) {
        timing = 500;
    }
    var animation_timing_ms = timing;
    var transitions = el.style['transition'];

    el.style['transition']          = 'opacity ' + animation_timing_ms + 'ms ease-in-out';
    el.style.opacity                = 0;  

    if(callback) {
        setTimeout(function(){
            el.style.transitions = transitions;
            callback();
        }, timing);
    }    
}