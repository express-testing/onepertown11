flashCore.prototype.iosSrcsetFix = function() {	
	if((/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) || navigator.userAgent.search("Safari") >= 0) {
		document.querySelectorAll('[srcset]').forEach(function(image){
			if(!image.parentNode.classList.contains('contain_image') && !image.parentNode.classList.contains('cover_image')) {
				image.parentNode.innerHTML = image.parentNode.innerHTML + '';
			}
		});
	}
}